/**
 * JSONSense plugin for html functionality
 */

let fs;
try{
    fs = require('fs')
}catch(e){
    fs = false
}

const jQuery = require('jquery')
const jsdom = require('jsdom')
const url = require('url')
const axios = require('axios');

RegExp.quote = function (str) {
    return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
}


//TODO:  Add alternate find function (query?) that allows for normal dom
//style queries.  #theId.classname .childclassname[data-custom="1"]
module.exports = {
    name: 'HTMLSense',
    install() {
        this.installed = true
    },
    generateGuid() {
        var result, i, j;
        result = '';
        for (j = 0; j < 32; j++) {
            if (j == 8 || j == 12 || j == 16 || j == 20)
                result = result + '-';
            i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
            result = result + i;
        }
        return result;
    },
    getAttributes(node) {
        let atts = {}
        for (let attKey in node.attributes) {
            atts[node.attributes[attKey].nodeName] = node.attributes[attKey].nodeValue
        }
        return this.copy(atts)
    },
    extractHTML(html, callback) {

        jsdom.env("", (err, window) => {
            if (err) {
                callback(err)
            } else {
                let $ = jQuery(window)
                let nodes = this._extractHTML(html, $)
                callback(null, nodes)
            }
        })
    },
    _extractHTML(html, $) {
        let nodes = []
        //first pass, convert to html
        let parsedHtml = (typeof html === 'string') ? $.parseHTML(html) : html
        $.each(parsedHtml, (i, el) => {
            nodes.push({
                tag: el.nodeName.toLowerCase(),
                atts: this.getAttributes(el),
                children: this._extractHTML(el.childNodes, $) || [],
                content: (el.nodeName === '#text') ? el.nodeValue : ''
            })
        })
        return nodes
    },
    replaceChildrenWithGuid(dom) {
        let newDom = dom
        newDom.forEach(el => {
            if (el.content) {
                el.children.forEach((child, childIndex) => {
                    el.content = el.content.replace(new RegExp(RegExp.quote(child.outerHTML), 'g'), child.guid)
                    delete child.outerHTML
                })
            }
            delete el.outerHTML
            if (el.children.length > 0)
                el.children = this.replaceChildrenWithGuid(el.children)
        })
        return newDom
    },
    getHTML(source, callback) {
        if (typeof source === 'string') {
            this.getHTMLFromPath(source, (err, data) => {
                if(source.indexOf('http') !== -1){
                    if (err) {
                        this.getHTMLFromURL(source, (err, data) => {
                            if (err) {
                                callback(err)
                            } else {
                                callback(null, data)
                            }
                        })
                    } else {
                        callback(null, data)
                    }
                }else{
                    callback('File not found')
                }

            })
        }
    },
    getHTMLFromURL(source, callback) {
        axios.get(source).then(function (response) {
            if (response.status === 200) {
                callback(null, response.data)
            } else {
                callback("status code " + response.status)
            }

        }).catch((err) => {
            callback(err)
        })
    },
    getHTMLFromPath(source, callback) {
        if(fs){
            fs.readFile(source, 'utf8', (err, data) => {
                if (err) {
                    callback(err)
                } else {
                    callback(null, data)
                }
            })
        }else{
            callback('fs does not exist')
        }

    },
    convertJSONToHTML(dom) {
        let newDom = this.copy(dom)
        let elements = []
        newDom.forEach((item) => {
            this.convertToOuterHTML(item)
            elements.push(item.outerHTML)
        })


        return elements.join('')
    },
    convertToOuterHTML(item) {
        if (Object.keys(item).length !== 0) { //error correction in case empty object
            let output = ""
            let childrenContent = ""
            if (item.children) {
                item.children.forEach((child) => {
                    this.convertToOuterHTML(child)
                    childrenContent += child.outerHTML
                })
            }
            if (item.tag !== "#text") {
                let selfClosing = ['area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr']
                let isSelfClosing = this.isArrayMatch(selfClosing, [item.tag])
                if (item.tag) {
                    output += '<' + item.tag + ''
                    for (let att in item.atts) {
                        output += " " + att + '="' + item.atts[att] + '"'
                    }
                }
                if (isSelfClosing && item.content === "" && item.tag) {
                    output += '>'
                    item.outerHTML = output
                    return output
                } else {
                    if (!item.content) item.content = ""
                    if (item.tag) output += '>'
                    output += item.content + childrenContent
                    if (item.tag) output += '</' + item.tag + '>'
                    item.outerHTML = output
                    return output
                }
            } else {
                item.outerHTML = item.content
                return item.outerHTML
            }
        }
    },
    replaceGuidWithChildren(dom) {
        dom.forEach(item => {
            this.replaceGuidWithChildren(item.children)
            item.children.forEach((child) => {
                this.convertToOuterHTML(child)
                item.content = item.content.replace(new RegExp(RegExp.quote(child.guid), 'g'), child.outerHTML)
            })
            this.convertToOuterHTML(item)
        })
    },
    convertHTMLToJSON(source, callback) {
        this.getHTML(source, (err, data) => {
            if (err) {
                callback(err)
            } else {
                this.extractHTML(data, (err, data) => {
                    if (err) {
                        callback(err)
                    } else {
                        let dom = data
                        // this.replaceChildrenWithGuid(dom)
                        callback(null, dom)
                    }
                })
            }
        })
    }
}