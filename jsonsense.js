/**
 * Performs query and modifications operations on JSON objects
 *
 * @class JSONSense
 */
class JSONSense {
    /**
     * @param {any} [options]
     * @memberOf JSONSense
     */
    constructor(options = {}) {
        this.equalityIgnoreKeys = options.equalityIgnoreKeys || []
        this.childKey = options.childKey || null
        this.dataObjectArray = []
        this.results = []
        this.pluginNames = []
    }

    /**
     * Concats this.dataObjectArray with provided array or Object
     * @param {(Object|Object[])} data
     * @memberOf JSONSense
     * @returns {JSONSense} this
     */
    addData(data) {

        this.dataObjectArray = this.dataObjectArray.concat(data)
        return this
    }

    /**
     * returns the data object that operations are run against
     * @returns {Object[]} JSONSense instance's dataObjectArray
     *
     * @memberOf JSONSense
     */
    getData() {
        return this.dataObjectArray
    }

    /**
     *  Finds an object based on pattern and returns reference
     * @param {any} pattern - pattern to search for
     * @memberOf JSONSense
     * @returns {JSONSense} this
     */
    find(pattern = {}, recursive = false, findInResults = false) {
        let results
        if (findInResults) {
            results = this._findIn(this.results, pattern, recursive)
        } else {
            results = this._findIn(this.dataObjectArray, pattern, recursive)
        }

        this.results = results

        return this
    }

    _findIn(input, pattern, recursive) {

        let results = []
        if (this.isObject(input)) {
            if (this.isObjectMatch(input, pattern, this.ignoreKeys)) {
                results.push(input)
            }
            if (recursive) {
                for (let key in input) {
                    results = results.concat(this._findIn(input[key], pattern, recursive))
                }
            }

        } else if (this.isArray(input)) {
            input.forEach((item) => {

                results = results.concat(this._findIn(item, pattern, recursive))
            })
        }
        return results
    }

    /**
     * Gets the latest find or mutation result
     * @memberOf JSONSense
     * @returns {Object[]} this.result - returns latest result
     */
    getResult() {
        return this.results
    }

    /**
     * Performs a deep copy of the object.  If undefined, uses this.result and returns this with modified result.
     * Otherwise it will return the new object copy.
     * @memberOf JSONSense
     * @param {any} [obj=this.result] - object or array to copy
     * @returns {(Object|Object[]|JSONSense)}
     */
    changeEach(obj, callback) {
        let lastArgument = arguments[arguments.length - 1]
        let selectedObjects = []
        if (typeof lastArgument === 'function') {
            selectedObjects = (this.isArray(arguments[0])) ? arguments[0] : this.results
            selectedObjects.forEach((object, index, array) => {
                lastArgument(object, index, array)
            })
        } else {
            console.error('change() last parameter must be a function')
        }
        return this
    }

    copy(obj) {
        let copy = obj
        if (typeof copy === 'undefined') {
            copy = this.results
            return JSON.parse(JSON.stringify(copy))
        } else {
            return JSON.parse(JSON.stringify(copy))
        }
    }

    /**
     * Tests variable to see if it's an object and not an array
     * @param {any} test - variable to test
     * @returns {Boolean} - is Object and not array
     * @memberOf JSONSense
     */
    isObject(test) {
        if (typeof test === 'undefined' || test === null) return false
        return (test.constructor === Object)
    }

    /**
     * Tests if variable is array
     * @param {any} test - variable to test
     * @returns {Boolean} - is array
     *
     * @memberOf JSONSense
     */
    isArray(test) {
        return Array.isArray(test)
    }

    /**
     * Testes variables to see if they match.  If match is null, the pattern will evaluate as true due to needing a wild card that wouldn't show up in json
     * @param {any} source - variable to test
     * @param {(String|Number|RegExp)} match - pattern to match against
     * @param {boolean} [matchType=true] - if false, converts numbers to string
     * @returns {Boolean} isMatch - are the variables matching
     *
     * @memberOf JSONSense
     */
    isMatch(source, match, matchType = true) {
        if (match === null) return true //null is used as wildcare, rather than regexp.

        if (typeof source === typeof match) return (source === match)
        // if(this.isObject(source) && this.isObject(match)) return this.isObjectMatch(source, match)
        if (typeof source === 'string' && typeof match === 'number' && !matchType) {
            return (source === match.toString())
        }
        if (typeof source === 'number' && typeof match === 'string' && !matchType) {
            return (source.toString() === match)
        }
        if (typeof source === 'string' && match instanceof RegExp) {
            return match.test(source)
        }
        if (!matchType) return (source == match)
    }

    /**
     * Tests to see if source contains containsList. This includes objectMatching patterns
     *
     * @param {Array} source - array to test against
     * @param {any} containsList - array of patterns to test for
     * @returns {Boolean} - if array is contained in the other
     *
     * @memberOf JSONSense
     */
    isArrayMatch(source, containsList) {
        //Array of required items not found
        let unmatched = containsList.filter((checkItem) => {
            let found = false
            source.forEach((item) => {
                if (this.isObject(checkItem) && this.isObject(item)) {
                    found = (this.isObjectMatch(item, checkItem)) ? true : found
                } else if (this.isArray(item) && this.isArray(checkItem)) {
                    found = (this.isArrayMatch(item, checkItem)) ? true : found
                } else {
                    found = (this.isMatch(item, checkItem)) ? true : found
                }
            })
            return !found
        })
        return (unmatched.length === 0)
    }

    /**
     * verifies whether or not an object matches a pattern
     * @param {Object} source - source object to test against
     * @param {Object} pattern - pattern to test against
     * @param {(String|Object)[]} ignoreKeys - Key string or object patterns to ignore.
     * @returns {Boolean} - Source matches pattern
     *
     * @memberOf JSONSense
     */
    isObjectMatch(source, pattern, ignoreKeys = this.equalityIgnoreKeys) {
        if (this.isObject(source) && this.isObject(pattern)) {
            for (let patternKey in pattern) {

                if (typeof source[patternKey] === 'undefined') return false
                if (this.isObject(pattern[patternKey]) && this.isObject(source[patternKey])) {
                    if (!this.isObjectMatch(source[patternKey], pattern[patternKey])) return false

                } else if (this.isArray(pattern[patternKey]) && this.isArray(source[patternKey])) {
                    if (!this.isArrayMatch(source[patternKey], pattern[patternKey])) return false

                } else {
                    if (!this.isMatch(source[patternKey], pattern[patternKey])) return false
                }
            }
        } else {
            return false
        }
        return true
    }

    //TODO: move toArray to the html pattern plugin
    /**
     *
     *
     * @param {any} obj
     * @returns
     *
     * @memberOf JSONSense
     */
    toArray(obj) {
        let array = [];
        // iterate backwards ensuring that length is an UInt32
        for (let i = obj.length >>> 0; i--;) {
            array[i] = obj[i];
        }
        return array;
    }

    /**
     * Deletes object, or keys based on keyname string or object pattern.
     * Deleted objects simply have their keys deleted.  Empty objects are removed from dataObjectArray recursively
     *
     * @param {(Object|Object[])} obj - object to perform delete operations on. uses this.result if no params passed, or null/false passed
     * @param {(String|Object)[]} [deleteKeys=[]] - delete keys matching string or object pattern
     * @returns {Object[]} - remaining
     *
     * @memberOf JSONSense
     */
    delete(obj, deleteKeys) {
        //TODO: extend delete functionality
        let selectedObjects = []
        if (typeof obj === 'undefined' || obj === null || obj === false) {
            selectedObjects = this.results
        } else if (!this.isArray(obj) && this.isObject(obj)) {
            selectedObjects.push(obj)
        } else if (this.isArray(obj)) {
            selectedObjects = obj
        } else {
            return console.error('provided non array, object, undefined, null, or false to delete()')
        }

        if (this.isArray(deleteKeys)) {
            selectedObjects.forEach((item) => {
                if (this.isObject(item)) {
                    deleteKeys.forEach((keyPattern) => {
                        if (typeof keyPattern === 'string' && item[keyPattern]) delete item[keyPattern]
                    })
                } else {
                    console.error('provided non-object to delete()')
                }
            })
        } else {
            selectedObjects.forEach((item) => {
                if (this.isObject(item)) {
                    for (let key in item) {
                        delete item[key]
                    }
                }
            })
        }
        this._deletedCleanup(false, selectedObjects)
        return this
    }

    /**
     * Utility.  Finds object references within dataObjectArray recursively and deletes them.
     * @param {(Object|Object[])} obj
     * @memberOf JSONSense
     */
    _deletedCleanup(source, deletedObjects) {
        if (!source) {
            source = this.dataObjectArray
        }
        source.forEach((item, dataIndex) => {
            deletedObjects.forEach((deleted, deletedIndex) => {
                if (item === deleted && Object.keys(item).length === 0) {
                    source.splice(dataIndex, 1)
                    deletedObjects.splice(deletedIndex, 1)
                } else {
                    for (let key in item) {
                        if (item[key] === deleted) {
                            delete item[key]
                        }
                    }
                }
            })
        })
        source.forEach((item) => {
            if (this.isObject(item)) {
                for (let key in item) {
                    if (this.isArray(item[key])) {
                        this._deletedCleanup(item[key], deletedObjects)
                    }
                }
            }
        })
    }

    /**
     * Applies object keys to this instance of JSONSense
     *
     * @param {Object} plugin - Plugin object with keys to map to instance.
     *
     * @memberOf JSONSense
     */

    use(plugin) {
        let installerExists = false
        for (let key in plugin) {
            if (key === 'name') {
                let name = plugin[key]
                this.pluginNames.push(name)
            } else if (key === 'install') {
                installerExists = true
            } else {
                JSONSense.prototype[key] = plugin[key]
            }

        }
        if (installerExists) plugin.install.bind(this)()
    }
}

module.exports = JSONSense