const JSONSense = require('../jsonsense')
const assert = require('assert')
const testJson = require('./test.json')
describe("JSONSense class", function () {
    describe("instantiation", function () {
        it("creates a new instance", function () {
            let jsonSense = new JSONSense()
            assert.equal(jsonSense.constructor, JSONSense)
        })
    });

    describe("addData()", function () {
        it("Adds an object to the dataObjectArray", function () {
            let jsonSense = new JSONSense()
            let newObject = {}
            jsonSense.addData(newObject)
            assert.equal(jsonSense.dataObjectArray[0], newObject)
        })
        it("Concatenates and array into dataObjectArray", function () {
            let jsonSense = new JSONSense()
            let newObject = {}
            let newArray = [newObject]
            jsonSense.addData(newArray)
            assert.equal(jsonSense.dataObjectArray[0], newObject)
        })
        it("returns an instance of this", function () {
            let jsonSense = new JSONSense()
            let newObject = {}
            let returnValue = jsonSense.addData(newObject)
            assert.equal(jsonSense, returnValue)
        })
    });

    describe("getData()", function () {
        it("returns the dataObjectArray", function () {
            let jsonSense = new JSONSense()
            let newObject = {}
            jsonSense.addData(newObject)
            let getData = jsonSense.getData()
            assert.equal(getData[0], newObject)
        })
    });
    describe("getResult()", function () {
        it("returns this.results[]", function () {
            let jsonSense = new JSONSense()
            let testResultsArray = [{}]
            jsonSense.results = testResultsArray
            assert.equal(jsonSense.getResult()[0], testResultsArray[0])
        })
    });

    describe("find()", function () {
        it("returns an instance of this", function () {
            let jsonSense = new JSONSense()
            assert.equal(jsonSense.find(), jsonSense)
        })
        it("finds pattern and changes results to reference found object", function () {
            let jsonSense = new JSONSense()

            jsonSense.addData(testJson)
            jsonSense.find({
                name: "username"
            })
            let getResult = jsonSense.getResult()
            getResult.forEach((item) => {
                assert.equal(item.name, "username")
            })

        })
        it("recursively finds patterns", function () {
            let jsonSense = new JSONSense()
            jsonSense.addData(testJson)

            jsonSense.find({
                name: "username"
            }, true)
            let getResult = jsonSense.getResult()
            getResult.forEach((item) => {
                assert.equal(item.name, "username")
            })
        })
        it("passes references of found objects", function () {
            let jsonSense = new JSONSense()
            let newObject = {}
            let getResult = jsonSense
                .addData(newObject)
                .find({})
                .getResult()
            assert.equal(getResult[0], newObject)
        })
    });
    describe("copy()", function () {
        it("returns a deep copy of a provided object", function () {
            let jsonSense = new JSONSense()
            let testObject = {}
            assert.notEqual(testObject, jsonSense.copy(testObject),
                "Returned value references provided object")
        })
        it("copies this.results if no params", function () {
            let jsonSense = new JSONSense()
            let testObject = {
                name: "my name"
            }
            jsonSense.results = [testObject]
            assert.notEqual(testObject, jsonSense.copy()[0],
                "Returned value references this.result object")
            assert.deepEqual(testObject, jsonSense.copy()[0],
                "Returned value is not deep equal to this.result object")
        })
    });

    describe("isObject()", function () {
        it("returns true if regular Object", function () {
            let jsonSense = new JSONSense()
            assert.ok(jsonSense.isObject({}),
                "Object does not have constructor of Object")
        })
        it("returns false if not regular Object", function () {
            let jsonSense = new JSONSense()
            assert.ok(!jsonSense.isObject([]),
                "Evaluated Array as object")
            assert.ok(!jsonSense.isObject(RegExp()),
                "Evaluated RegExp as object")
            assert.ok(!jsonSense.isObject(function () {}),
                "Evaluated function as object")
        })
    });

    describe("isArray()", function () {
        it("return true if Array", function () {
            let jsonSense = new JSONSense()
            assert.ok(jsonSense.isArray([]),
                "Object does not have constructor of Object")
            assert.ok(!jsonSense.isArray({}),
                "Evaluated Object as an Array")
        })
        it("return false if not Array", function () {
            let jsonSense = new JSONSense()
            assert.ok(!jsonSense.isArray({}),
                "Evaluated Object as an Array")
        })
    });

    describe("isMatch()", function () {
        it("return true if null is match parameter", function () {
            let jsonSense = new JSONSense()
            assert.ok(jsonSense.isMatch(1, null),
                "Null not matching")
            assert.ok(jsonSense.isMatch("1", null),
                "Null not matching")
        })

        it("return true if values type and value match", function () {
            let jsonSense = new JSONSense()
            assert.ok(jsonSense.isMatch(1, 1),
                "Not matching equal numbers")
            assert.ok(jsonSense.isMatch("hello", "hello"),
                "Not matching equal strings")
            assert.ok(jsonSense.isMatch(true, true),
                "Not matching equal strings")
        })
        it("return true if values match and matchtype false", function () {
            let jsonSense = new JSONSense()
            assert.ok(jsonSense.isMatch(1, "1", false),
                "Didn't convert number to string")
            assert.ok(jsonSense.isMatch(0, "0", false),
                "Didn't convert number to string")
        })
        it("return false if values don't match", function () {
            let jsonSense = new JSONSense()
            assert.ok(!jsonSense.isMatch(1, 2),
                "Evaluated numbers as matching")
            assert.ok(!jsonSense.isMatch("hi", "there"),
                "Evaluated strings as matching")
            assert.ok(!jsonSense.isMatch({}, "there"),
                "Evaluated strings as matching")
            assert.ok(!jsonSense.isMatch({}, {}),
                "Evaluated different objects as match")
        })
    });

    describe("isArrayMatch()", function () {
        it("return true if array contains values of other", function () {
            let jsonSense = new JSONSense()
            assert.ok(jsonSense.isArrayMatch([1, 2, 3, 4, 5, 6], [1, 3, 6]),
                "Evaluated matching array as nonMatch")
            assert.ok(jsonSense.isArrayMatch([1, 2, 3, 4, 5, 6, "hello"], ["hello", 3, 6]),
                "Evaluated matching array as nonMatch")
        })
        it("returns true if object pattern match", function () {
            let jsonSense = new JSONSense()
            assert.ok(jsonSense.isArrayMatch([1, 2, {
                    name: 'myname',
                    age: 23
                }], [1, 2, {
                    name: 'myname'
                }]),
                "Evaluated array with missing value as matching")
        })
        it("returns true if multidimensional Array pattern match", function () {
            let jsonSense = new JSONSense()
            assert.ok(jsonSense.isArrayMatch([1, 2, [3, 4, 5]], [1, [4, 5]]),
                "Evaluated array with missing value as matching")
        })
        it("return false if array doesn't contain values of other", function () {
            let jsonSense = new JSONSense()
            assert.ok(!jsonSense.isArrayMatch([1, 2, 3, 4, 5, 6], [1, 3, "goodbye", 7]),
                "Evaluated array with missing value as matching")
        })
    });

    describe("isObjectMatch()", function () {
        it("return true if object matches pattern", function () {
            let jsonSense = new JSONSense()
            assert.ok(jsonSense.isObjectMatch({
                    name: "myname",
                    age: 26,
                    children: [{
                        name: "child1",
                        age: 20,
                        children: []
                    }, {
                        name: "child2",
                        age: 20,
                        children: []
                    }]
                }, {
                    name: "myname"
                }),
                "Not matching object to pattern")
        });
        it("return true if object matches arrays", function () {
            let jsonSense = new JSONSense()
            assert.ok(jsonSense.isObjectMatch({
                    name: "myname",
                    age: 26,
                    children: [{
                        name: "child1",
                        age: 20,
                        children: []
                    }, {
                        name: "child2",
                        age: 20,
                        children: []
                    }]
                }, {
                    children: [{
                        name: "child2"
                    }]
                }),
                "Not matching object with array")
        });
        it("return true if object matches with key as object", function () {
            let jsonSense = new JSONSense()
            assert.ok(jsonSense.isObjectMatch({
                    name: "myname",
                    age: 26,
                    child: {
                        name: "childname"
                    }
                }, {
                    child: {
                        name: "childname"
                    }
                }),
                "Not matching key as object")
        });
        it("return false if no match", function () {
            let jsonSense = new JSONSense()
            assert.ok(!jsonSense.isObjectMatch({
                    name: "myname",
                    age: 26
                }, {
                    name: "noname"
                }),
                "Treating non-match as matching")
        });
    });

    describe("delete()", function () {
        it("if no object passed (false, null, undefined), delete objects in this.results", function () {
            let jsonSense = new JSONSense()
            jsonSense.addData([{
                id: 1
            }, {
                id: 2
            }, {
                id: 3
            }])
            jsonSense.find({
                id: 1
            }).delete()
            assert.ok(jsonSense.isArrayMatch(jsonSense.getData(), [{
                    id: 2
                }, {
                    id: 3
                }]),
                "items left after delete not found in getData()")
            assert.ok(!jsonSense.isArrayMatch(jsonSense.getData(), [{
                    id: 1
                }]),
                "Removed item found in getData()")
        });
        it("if no deleteKeys passed delete objects", function () {
            let jsonSense = new JSONSense()
            jsonSense.addData([{
                id: 1
            }, {
                id: 2
            }, {
                id: 3
            }])
            jsonSense.find({
                id: 1
            }).delete()
            assert.ok(jsonSense.isArrayMatch(jsonSense.getData(), [{
                    id: 2
                }, {
                    id: 3
                }]),
                "items left after delete not found in getData()")
            assert.ok(!jsonSense.isArrayMatch(jsonSense.getData(), [{
                    id: 1
                }]),
                "Removed item found in getData()")
        });
        it("removes deleted objects from data after they are cleared out", function () {
            let jsonSense = new JSONSense()
            let itemToDelete = {
                id: 2
            }

            jsonSense.addData([{
                id: 1
            }, itemToDelete, {
                id: 3
            }])

            jsonSense.find({
                id: 2
            }).delete()

            assert.notEqual(jsonSense.getData()[1].id, 2,
                "Deleted tag still in object")
        });
    });
    describe("changeEach()", function () {
        it("should use this.results if no objects passed", function () {
            let jsonSense = new JSONSense()
            jsonSense.addData([{
                id: 1
            }, {
                id: 2
            }, {
                id: 3
            }])
            jsonSense.find().changeEach((item, index) => {
                let data = jsonSense.getData()
                assert.equal(data[index], item, "not matching this.results array")
            })
        })
        it("should use pass each object to callback", function () {
            let jsonSense = new JSONSense()

            jsonSense.addData([{
                id: 1
            }, {
                id: 2
            }, {
                id: 3
            }])
            jsonSense.find().changeEach((item) => {
                item.newKey = true
            })
            jsonSense.getData().forEach((item) => {
                assert.equal(item.newKey, true, 'modification through callback not working')
            })
        })
        it("should accept an object array as first argument", function () {
            let jsonSense = new JSONSense()
            let data = [{
                id: 1
            }, {
                id: 2
            }, {
                id: 3
            }]
            jsonSense.changeEach(data, (item, index) => {
                assert.equal(data[index], item, "not matching passed object array")
            })
            assert.ok(jsonSense.getData().length === 0, "using this.results instead of passed object array")
        })
    });

    describe("use() - plugin functionality that accepts object", function () {
        it("should add prototypes of passed function to instance", function () {
            let jsonSense = new JSONSense()
            let plugin = {
                pluginVariable: 2,
                newFunction() {
                    return this.getData()
                }
            }
            jsonSense.use(plugin)
            jsonSense.addData({id:1})
            jsonSense.pluginVariable++
            assert.equal(jsonSense.newFunction()[0].id, 1, 'plugin cant use this context')
            assert.equal(jsonSense.pluginVariable, 3, 'plugin not adding this.variable')
        });
        it("should add the pluginName to prototype list on instance", function () {
            let jsonSense = new JSONSense()
            let plugin = {
                name: 'myplugin',
                pluginVariable: 2,
                newFunction() {
                    return this.getData()
                }
            }
            jsonSense.use(plugin)
            assert.equal(jsonSense.pluginNames[0], 'myplugin', 'plugin name not getting added to array')
        });
        it("should run the plugin installer in this context", function () {
            let jsonSense = new JSONSense()
            let plugin = {
                name: 'myplugin',
                install(){
                    this.newVariable = "newvar"
                },
                pluginVariable: 2,
                newFunction() {
                    return this.getData()
                }
            }
            jsonSense.use(plugin)
            assert.equal(jsonSense.newVariable, 'newvar', 'not running installer or not running in this context')
        });
    });


});