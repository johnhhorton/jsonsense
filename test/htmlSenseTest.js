const JSONSense = require('../jsonsense')
const htmlSense = require('../htmlSense')
const assert = require('assert')
const testJson = require('./test.json')


RegExp.quote = function (str) {
    return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
}

describe("htmlSense plugin", function () {
    describe("installation", function () {
        it("should install", function () {
            let jsonSense = new JSONSense()
            jsonSense.use(htmlSense)
            assert.equal(jsonSense.pluginNames[0], 'HTMLSense')
        })
    });

    // describe("generateGuid()", function () {
    //     it("should generate a Guid", function () {
    //         let jsonSense = new JSONSense()
    //         jsonSense.use(htmlSense)

    //         let guid1 = jsonSense.generateGuid()
    //         let guid2 = jsonSense.generateGuid()
    //         assert.ok(guid1, "guid not created")
    //         assert.ok(guid2, "guid not created")
    //         assert.notEqual(guid1, guid2, "guids are equal")
    //     })
    // });

    describe("getHtml()", function () {
        it("Should return html if url", (done) => {
            let jsonSense = new JSONSense()
            jsonSense.use(htmlSense)
            jsonSense.getHTML('https://google.com', (err, data) => {
                assert.ok(data)
                done()
            })
        })
        it("Should return error if bad url", (done) => {
            let jsonSense = new JSONSense()
            jsonSense.use(htmlSense)
            jsonSense.getHTML('ergerg', (err, data) => {
                assert.ok(err)
                assert.ok(!data)
                done()
            })
        })
        it("Should return html if valid file", (done) => {
            let jsonSense = new JSONSense()
            jsonSense.use(htmlSense)
            jsonSense.getHTML(__dirname + '/accident-at-work.html', (err, data) => {
                assert.ok(data)
                done()
            })
        })
        it("Should return error if file not found", (done) => {
            let jsonSense = new JSONSense()
            jsonSense.use(htmlSense)
            jsonSense.getHTML(__dirname + '/accident-at-work-fake.html', (err, data) => {
                assert.ok(!data)
                assert.ok(err)
                done()
            })
        })
    });

    describe("extractHtml()", function () {
        it("Should convert html to json", (done) => {
            let jsonSense = new JSONSense()
            jsonSense.use(htmlSense)
            jsonSense.extractHTML(`
            <div class="parent">
            <div class="child">
            <h1>the title</h1>
            <ul>
            <li>list item 1</li>
            <li>list item 2</li>
            </ul></div>
            </div>`, (err, data) => {
                assert.ok(jsonSense.isArray(data[0].children))
                done()
            })
        })
    });

    // describe("replaceChildrenWithGuid()", function () {

    //     it("Should replace content matching children with guid", (done) => {
    //         let jsonSense = new JSONSense()
    //         jsonSense.use(htmlSense)
    //         jsonSense.extractHTML(`
    //         <div class="parent">
    //         <div class="child">
    //         <h1>the title</h1>
    //         <ul>
    //         <li>list item 1</li>
    //         <li>list item 2</li>
    //         </ul></div>
    //         </div>`, (err, data) => {
    //             data = jsonSense.replaceChildrenWithGuid(data)
    //             let childGuid = data[0].children[0].guid
    //             let parentContent = data[0].content
    //             assert.ok(parentContent.includes(childGuid) !== -1)
    //             done()
    //         })
    //     })
    // });
//     describe("replaceGuidWithChildren()", function () {

//         it("Should convert JSON back to HTML", (done) => {
//             let jsonSense = new JSONSense()
//             jsonSense.use(htmlSense)
//             let dom = [{
//                 "tag": "div",
//                 "atts": {
//                     "class": "parent"
//                 },
//                 "children": [{
//                     "tag": "div",
//                     "atts": {
//                         "class": "child"
//                     },
//                     "children": [{
//                         "tag": "h1",
//                         "atts": {},
//                         "children": [],
//                         "content": "the title",
//                         "guid": "A4B94EA6-CB1E-3B0F-C3E9-376F975C057C",
//                     }, {
//                         "tag": "ul",
//                         "atts": {},
//                         "children": [{
//                             "tag": "li",
//                             "atts": {},
//                             "children": [],
//                             "content": "list item 1",
//                             "guid": "970F9CBD-AD1C-A8F0-D6CE-418986891549",
//                         }, {
//                             "tag": "li",
//                             "atts": {},
//                             "children": [],
//                             "content": "list item 2",
//                             "guid": "389935B9-4B0A-5151-7BA7-7362C3221D31",
//                         }],
//                         "content": "\n            970F9CBD-AD1C-A8F0-D6CE-418986891549\n            389935B9-4B0A-5151-7BA7-7362C3221D31\n            ",
//                         "guid": "BB1B88E5-A6B0-1D9D-EE1E-F04A32F460C2",
//                     }],
//                     "content": "\n A4B94EA6-CB1E-3B0F-C3E9-376F975C057C\n            BB1B88E5-A6B0-1D9D-EE1E-F04A32F460C2",
//                     "guid": "80A3772A-1E61-D8F1-5F22-C03B27019B72",
//                 }],
//                 "content": "\n            80A3772A-1E61-D8F1-5F22-C03B27019B72\n            ",
//                 "guid": "728DBE65-A69C-7DCA-B3BF-E430D2426AB2"
//             }]

//             jsonSense.replaceGuidWithChildren(dom)
//             assert.equal(dom[0].outerHTML, `<div class="parent">
//             <div class="child">
//  <h1>the title</h1>
//             <ul>
//             <li>list item 1</li>
//             <li>list item 2</li>
//             </ul></div>
//             </div>`)
//             done()
//         })
//     });

    describe("convertToOuterHTML()", function () {

        it("should convert item to outerHtml key of the item", (done) => {
            let jsonSense = new JSONSense()
            jsonSense.use(htmlSense)
            let dom = [{
                "tag": "div",
                "atts": {
                    "class": "parent",
                    "id": "theId",
                },
                "content":"",
                "children":[
                    {
                        tag: '#text',
                        atts:{},
                        content:"Hello World"
                    }
                ]
            }]
            let result = jsonSense.convertToOuterHTML(dom[0])
            assert.equal(result, '<div class="parent" id="theId">Hello World</div>')
            done()
        })

        it("should correctly convert self closing items", (done) => {
            let jsonSense = new JSONSense()
            jsonSense.use(htmlSense)
            let dom = [{
                "tag": "img",
                "atts": {
                    "class": "myImg",
                    "id": "theImg",
                    "src": "http://img.com",
                    "alt": "alt text"
                },
                "content": "",
                "guid": "728DBE65-A69C-7DCA-B3BF-E430D2426AB2"
            }]
            let result = jsonSense.convertToOuterHTML(dom[0])
            assert.equal(result, '<img class="myImg" id="theImg" src="http://img.com" alt="alt text" />')
            done()
        })
    });

    describe("convertJSONToHTML()", function () {

        it("Should perform output conversion and output as html", (done) => {
            let jsonSense = new JSONSense()
            jsonSense.use(htmlSense)
            let dom = [{
                "tag": "div",
                "atts": {
                    "class": "parent"
                },
                "children": [{
                    "tag": "div",
                    "atts": {
                        "class": "child"
                    },
                    "children": [{
                        "tag": "h1",
                        "atts": {},
                        "children": [{tag:"#text", atts:{}, content:"the title"}],
                        "content": ""
                    }, {
                        "tag": "ul",
                        "atts": {},
                        "children": [{
                            "tag": "li",
                            "atts": {},
                            "children": [{tag:"#text", atts:{}, content:"list item 1"}],
                            "content": ""
                        }, {
                            "tag": "li",
                            "atts": {},
                            "children": [{tag:"#text", atts:{}, content:"list item 2"}],
                            "content": ""
                        }],
                        "content": ""
                    }],
                    "content": ""
                }],
                "content": ""
            }]

            let newDom = jsonSense.convertJSONToHTML(dom)
            assert.equal(newDom, `<div class="parent"><div class="child"><h1>the title</h1><ul><li>list item 1</li><li>list item 2</li></ul></div></div>`)
            done()
        })
    });

    describe("Test of full stack", function () {

        it("Should perform all operations", (done) => {
            let jsonSense = new JSONSense()
            jsonSense.use(htmlSense)
            jsonSense.convertHTMLToJSON(__dirname + '/test.html', (err, data) => {
                assert.ok(!err)
                assert.ok(data)
                let html = jsonSense.convertJSONToHTML(data)
                assert.equal(html, `<div class="parent">
    <div class="child">
        <h1>the title</h1>
        <ul>
            <li>list item 1</li>
            <li>list item 2</li>
        </ul>
    </div>
</div>`)
                done()
            })


        })
    });
});